# 👏 Assignment 5 Springboot 

This is assignment 5 for Experis Academy, it is made in Java, using Springboot, Docker and Postgres.

### 🔥 Background

The purpose of this project is to create an api using springboot. The project is deployed on Heroku, using Github CI/CD pipelines.

### 🤖 Technologies

* Java jdk-17
* Intellij Idea
* Springboot
* Gradle
* Postgres
* Heroku
* Docker
* Gitlab CI/CD

### ⚠️Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

If you want the full project, simply clone the repo below: 
```
git clone https://gitlab.com/simonroshall/movie_character_api.git
```

### 💻 Executing program

How to run the application, go to the following url and use the endpoints in the project (for example /api/v1/movies):
```
* https://movie-characters-api-docker.herokuapp.com/'
```
### 😎 Contributors

Contributors and contact information

Ahmed Ali
[@ahmedabdiali](https://gitlab.com/ahmedabdiali)

Simon Roshäll 
[@simonroshall](https://gitlab.com/simonroshall/)


### 🌌 Contributing

PRs are not accepted

## 🦝 Version History

* 0.1
  * Initial Release
