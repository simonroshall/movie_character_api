package experis.movie_characters_api.services.movie;

import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.services.CrudService;

public interface MovieService extends CrudService<Movie, Integer> {
}