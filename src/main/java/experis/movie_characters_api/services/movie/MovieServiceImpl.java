package experis.movie_characters_api.services.movie;

import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.repositories.MovieRepository;
import experis.movie_characters_api.services.exceptions.MovieNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }
    @Override
    public Movie add(Movie movie) {
        return movieRepository.save(movie);
    }
    @Override
    public Movie update(Movie movie) {
        return movieRepository.save(movie);
    }
    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }
}
