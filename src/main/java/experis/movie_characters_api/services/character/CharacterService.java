package experis.movie_characters_api.services.character;

import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.services.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {
}