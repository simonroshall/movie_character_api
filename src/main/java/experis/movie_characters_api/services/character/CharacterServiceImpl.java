package experis.movie_characters_api.services.character;

import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.repositories.CharacterRepository;
import experis.movie_characters_api.services.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }
    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }
    @Override
    public Character add(Character character) {
        return characterRepository.save(character);
    }
    @Override
    public Character update(Character character) {
        return characterRepository.save(character);
    }
    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

}
