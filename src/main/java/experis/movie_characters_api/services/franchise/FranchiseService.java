package experis.movie_characters_api.services.franchise;

import experis.movie_characters_api.models.Franchise;
import experis.movie_characters_api.services.CrudService;

public interface FranchiseService extends CrudService<Franchise, Integer> {
}