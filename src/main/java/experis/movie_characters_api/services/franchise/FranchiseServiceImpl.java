package experis.movie_characters_api.services.franchise;

import experis.movie_characters_api.models.Franchise;
import experis.movie_characters_api.repositories.FranchiseRepository;
import experis.movie_characters_api.services.exceptions.FranchiseNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }
    @Override
    public Franchise add(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }
    @Override
    public Franchise update(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }
    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }
}
