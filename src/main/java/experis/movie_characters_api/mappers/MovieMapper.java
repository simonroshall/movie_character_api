package experis.movie_characters_api.mappers;

import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.models.Franchise;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CreateMovieDTO;
import experis.movie_characters_api.models.dtos.MovieDTO;
import experis.movie_characters_api.services.character.CharacterService;
import experis.movie_characters_api.services.franchise.FranchiseService;
import experis.movie_characters_api.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected MovieService movieService;
    //Mapping domain movie to a movie DTO
    @Mapping(target = "franchises", source = "franchises.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);
    //Mapping a set of movies to a set of movie DTOs
    public abstract Collection<MovieDTO> moviesToMoviesDto(Collection<Movie> movies);
    //Mapping a movie DTO to a domain movie
    @Mapping(target = "franchises", source = "franchises", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdToCharacter")
    public abstract Movie movieDtoToMovie(MovieDTO dto);
    //Mapping a create movie DTO to a domain movie
    @Mapping(target = "franchises", ignore = true)
    @Mapping(target = "characters", ignore = true)
    public abstract Movie movieDtoToMovie(CreateMovieDTO dto);
    //Method to get franchise id
    @Named("franchiseIdToFranchise")
    Franchise IdToFranchise(int id){return franchiseService.findById(id);}
    //Method to get character id
    @Named("characterIdToCharacter")
    Character IdToCharacter(int id){return characterService.findById(id);}
    //Method to get character ids for a franchise
    @Named("charactersToIds")
    Set<Integer> mapCharactersToId (Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(c -> c.getId()).collect(Collectors.toSet());
    }


}
