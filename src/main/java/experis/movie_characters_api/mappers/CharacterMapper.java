package experis.movie_characters_api.mappers;

import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CharacterDTO;
import experis.movie_characters_api.models.dtos.CreateCharacterDTO;
import experis.movie_characters_api.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService moviesService;

    //Mapping a domain character to a DTO
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);
    //Mapping a set of domain characters to a set of DTO characters
    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);
    //Mapping a DTO character to a domain character
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
    public abstract Character characterDtoToCharacter(CharacterDTO dto);
    @Mapping(target="movies", ignore = true)
    public abstract Character characterDTOToCharacter(CreateCharacterDTO dto);
    //Method to get movie ids that a character contains
    @Named("movieIdToMovies")
    Movie mapIdToMovies(int id) {
        return moviesService.findById(id);
    }
    //Method to get a set of movie ids. This is added to the character DTO.
    @Named("moviesToIds")
    Set<Integer> moviesToIds (Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}