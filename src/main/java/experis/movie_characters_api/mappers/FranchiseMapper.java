package experis.movie_characters_api.mappers;

import experis.movie_characters_api.models.Franchise;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CreateFranchiseDTO;
import experis.movie_characters_api.models.dtos.FranchiseDTO;
import experis.movie_characters_api.services.character.CharacterService;
import experis.movie_characters_api.services.franchise.FranchiseService;
import experis.movie_characters_api.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    MovieService movieService;
    @Autowired
    CharacterService characterService;
    @Autowired
    FranchiseService franchiseService;

    //Mapping a franchise to a franchise DTO
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToIds")
    public abstract FranchiseDTO franchisesToFranchisesDto(Franchise franchise);
    //Mapping a set of franchises to a set of DTO franchises
    public abstract Collection<FranchiseDTO> franchisesToFranchisesDto(Collection<Franchise> franchise);
    //Mapping a franchise DTO to a domain DTO
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
    public abstract Franchise franchisesDtoToFranchises(FranchiseDTO dto);
    //mapping a create franchise dto to a domain dto
    @Mapping(target = "movies", ignore = true)
    public abstract Franchise franchisesDtoToFranchises(CreateFranchiseDTO dto);
    //Method to get movie id for a franchise
    @Named("movieIdToMovies")
    Movie mapIdToMovies(int id) {
        return movieService.findById(id);
    }
    //Method to get movie ids for a franchise
    @Named("movieToIds")
    Set<Integer> mapMoviesToIds (Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
