package experis.movie_characters_api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name ="movie_title", length = 50)
    private String title;
    @Column(name ="movie_genre", length = 50)
    private String genre;
    @Column(name ="movie_release_year")
    private Integer releaseYear;
    @Column(name ="movie_director", length = 50)
    private String director;
    @Column(name ="movie_picture")
    private String picture;
    @Column(name ="movie_trailer")
    private String trailer;
    @ManyToOne
    @JoinColumn(name = "franchise_id", nullable = true)
    private Franchise franchises;
    @ManyToMany()
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Character> characters;
}

