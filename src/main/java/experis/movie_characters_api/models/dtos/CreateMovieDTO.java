package experis.movie_characters_api.models.dtos;

import lombok.Data;

@Data
public class CreateMovieDTO {
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;
    }
