package experis.movie_characters_api.models.dtos;

import lombok.Data;

@Data
public class CreateFranchiseDTO {
    private String name;
    private String description;
    private String director;
}
