package experis.movie_characters_api.models.dtos;

import lombok.Data;

@Data
public class CreateCharacterDTO {
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
