package experis.movie_characters_api.models.dtos;

import experis.movie_characters_api.models.Movie;
import lombok.Data;

import java.util.Set;
@Data
public class CharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
    private Set<Integer> movies;
}
