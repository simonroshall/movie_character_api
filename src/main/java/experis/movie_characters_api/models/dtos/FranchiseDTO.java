package experis.movie_characters_api.models.dtos;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
    private String director;
    private Set<Integer> movies;
}
