package experis.movie_characters_api.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name ="franchise_name", length = 50, nullable = false)
    private String name;
    @Column(name ="franchise_description", length = 150)
    private String description;
    @Column(name ="franchise_director", length = 50)
    private String director;
    @OneToMany(mappedBy = "franchises")
    private Set<Movie> movies;
}

