package experis.movie_characters_api.controllers;

import experis.movie_characters_api.mappers.CharacterMapper;
import experis.movie_characters_api.mappers.MovieMapper;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CharacterDTO;
import experis.movie_characters_api.models.dtos.CreateMovieDTO;
import experis.movie_characters_api.models.dtos.FranchiseDTO;
import experis.movie_characters_api.models.dtos.MovieDTO;
import experis.movie_characters_api.services.movie.MovieService;
import experis.movie_characters_api.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "/api/v1/movies")
@Tag(name = "Movie")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    //Customized swagger documentation. Responding with the specific DTO schema if the request was successful or a custom Api Error Response schema if the request failed.
    //These are made for all methods in the controllers.
    @Operation(summary = "get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the movies",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Find all movies.
    @GetMapping
    public ResponseEntity findAll() {
        Collection<MovieDTO> movies = movieMapper.moviesToMoviesDto(movieService.findAll());
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "get movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Find a movie by id.
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "add a new movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Add a new movie.
    @PostMapping
    public ResponseEntity addMovie(@RequestBody CreateMovieDTO newMovie) {
        Movie oneMovie = movieMapper.movieDtoToMovie(newMovie);
        Movie movie = movieService.add(oneMovie);
        URI uri = URI.create("movies/" + movie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "update a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    //Update an existing movie.
    @PutMapping("/{id}")
    public ResponseEntity updateMovie(@RequestBody MovieDTO movieDTO, @PathVariable int id){
        if (id != movieDTO.getId())
            return ResponseEntity.badRequest().build();
        movieService.update(
                movieMapper.movieDtoToMovie(movieDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the movie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Delete a movie by id.
    @DeleteMapping("{id}")
    public ResponseEntity deleteMovie(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "get characters in a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the characters",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Find characters in a movie by id.
    @GetMapping("/{id}/characters")
    public ResponseEntity findCharactersWithId(@PathVariable int id){
        Collection<CharacterDTO> charactersById = characterMapper.characterToCharacterDto(
            movieService.findById(id).getCharacters());
            return ResponseEntity.ok(charactersById);
    }
}
