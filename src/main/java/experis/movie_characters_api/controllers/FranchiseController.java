package experis.movie_characters_api.controllers;

import experis.movie_characters_api.mappers.CharacterMapper;
import experis.movie_characters_api.mappers.FranchiseMapper;
import experis.movie_characters_api.mappers.MovieMapper;
import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.models.Franchise;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CharacterDTO;
import experis.movie_characters_api.models.dtos.CreateFranchiseDTO;
import experis.movie_characters_api.models.dtos.FranchiseDTO;
import experis.movie_characters_api.models.dtos.MovieDTO;
import experis.movie_characters_api.services.franchise.FranchiseService;
import experis.movie_characters_api.services.movie.MovieService;
import experis.movie_characters_api.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/v1/franchises")
@Tag(name = "Franchise")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;
    private final MovieService movieService;
    protected final FranchiseMapper franchisesMapper;

    public FranchiseController(FranchiseService franchiseService, MovieMapper movieMapper, CharacterMapper characterMapper, MovieService movieService, FranchiseMapper franchisesMapper) {
        this.franchiseService = franchiseService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
        this.movieService = movieService;
        this.franchisesMapper = franchisesMapper;
    }

    //Customized swagger documentation. Responding with the specific DTO schema if the request was successful or a custom Api Error Response schema if the request failed.
    //These are made for all methods in the controllers.
    @Operation(summary = "get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the franchises",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to find all franchises.
    @GetMapping
    public ResponseEntity findAll() {
        Collection<FranchiseDTO> franchise = franchisesMapper.franchisesToFranchisesDto(franchiseService.findAll());
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "get franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to find a franchise by id.
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        FranchiseDTO franchise = franchisesMapper.franchisesToFranchisesDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "add new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateFranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to add a new franchise.
    @PostMapping
    public ResponseEntity addFranchise(@RequestBody CreateFranchiseDTO newFranchise) {
        Franchise oneFranchise = franchisesMapper.franchisesDtoToFranchises(newFranchise);
        Franchise franchise = franchiseService.add(oneFranchise);
            URI uri = URI.create("franchises/" + franchise.getId());
            return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "update a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    //Method to update an existing franchise.
    @PutMapping("/{id}")
    public ResponseEntity updateFranchise(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id){
        if (id != franchiseDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchisesMapper.franchisesDtoToFranchises(franchiseDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                            )}),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the franchises",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to delete a franchise. If there are any movies in the specific franchise, their ids is set to null.
    @DeleteMapping("{id}")
    public ResponseEntity deleteFranchise(@PathVariable int id) {
        var moviesSet = franchiseService.findById(id).getMovies();
        for (Movie movie : moviesSet) {
            if (movie.getId() == id) {
                movie.setFranchises(null);
            }
        }
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "get movies in franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get movies in the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Find a movies in a franchise by id.
    @GetMapping("/{id}/movies")
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<MovieDTO> moviesById = movieMapper.moviesToMoviesDto(
                franchiseService.findById(id).getMovies()
        );
        return ResponseEntity.ok(moviesById);
    }

    @Operation(summary = "get characters in a movie/franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the characters",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Find characters that's in a movie of the specific franchise.
    @GetMapping("/{id}/movies/characters")
    public ResponseEntity findCharacterWithIdInMovie(@PathVariable int id){
        var moviesFromFranchise = franchiseService.findById(id).getMovies();
        Set<Character> collection = new HashSet<>();

        for (Movie movies : moviesFromFranchise) {
            collection.addAll(movies.getCharacters());
        }
        Collection<CharacterDTO> characterFromFranchise = characterMapper.characterToCharacterDto(collection);
        return ResponseEntity.ok(characterFromFranchise);
    }
}
