package experis.movie_characters_api.controllers;

import experis.movie_characters_api.mappers.CharacterMapper;
import experis.movie_characters_api.mappers.MovieMapper;
import experis.movie_characters_api.models.Character;
import experis.movie_characters_api.models.Movie;
import experis.movie_characters_api.models.dtos.CharacterDTO;
import experis.movie_characters_api.models.dtos.CreateCharacterDTO;
import experis.movie_characters_api.models.dtos.MovieDTO;
import experis.movie_characters_api.services.character.CharacterService;
import experis.movie_characters_api.services.movie.MovieService;
import experis.movie_characters_api.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/characters")
@Tag(name = "Character")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;
    private final MovieMapper movieMapper;
    private final MovieService movieService;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, MovieMapper movieMapper, MovieService movieService) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieMapper = movieMapper;
        this.movieService = movieService;
    }

    //Customized swagger documentation. Responding with the specific DTO schema if the request was successful or a custom Api Error Response schema if the request failed.
    //These are made for all methods in the controllers.
    @Operation(summary = "get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the characters",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to find all customers.
    @GetMapping
    public ResponseEntity findAll() {
        Collection<CharacterDTO> characters = characterMapper.characterToCharacterDto(characterService.findAll());
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "get character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the character with the id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to find a specific character by id.
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        CharacterDTO characters = characterMapper.characterToCharacterDto(characterService.findById(id));
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "add new character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateCharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to add a new character.
    @PostMapping
    public ResponseEntity addCharacter(@RequestBody CreateCharacterDTO newCharacter) {
        Character oneCharacter = characterMapper.characterDTOToCharacter(newCharacter);
        Character character = characterService.add(oneCharacter);
        URI uri = URI.create("characters/" + character.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "update a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    //Method to update an existing character
    @PutMapping("/{id}")
    public ResponseEntity updateCharacter(@RequestBody CharacterDTO characterDTO, @PathVariable int id){
        if (id != characterDTO.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterDtoToCharacter(characterDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the character",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to delete a character. To be able to delete a character, we firstly need to delete the existing foreign keys (movies in characters and character in movies)
    @DeleteMapping("{id}")
    public ResponseEntity deleteCharacter(@PathVariable Integer id) {
        var moviesSet = characterService.findById(id).getMovies();
        for (Movie movie : moviesSet) {
            var movieCharacters = movie.getCharacters();
            Set<Character> characters = new HashSet<>();
            movieCharacters.forEach(x -> {if(x != characterService.findById(id)) characters.add(x);
            });
            movie.setCharacters(characters);
        }
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "get movies from character id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the movies with the id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    //Method to find all movies that a specific character is in.
    @GetMapping("/{id}/movies")
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<MovieDTO> moviesById = movieMapper.moviesToMoviesDto(
                characterService.findById(id).getMovies()
        );
        return ResponseEntity.ok(moviesById);
    }
}
