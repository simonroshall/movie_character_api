package experis.movie_characters_api.repositories;

import experis.movie_characters_api.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Integer> {
}
