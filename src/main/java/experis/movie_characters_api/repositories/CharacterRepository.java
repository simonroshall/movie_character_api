package experis.movie_characters_api.repositories;

import experis.movie_characters_api.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character,Integer> {
}
