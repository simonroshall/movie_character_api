INSERT INTO FRANCHISE (franchise_name, franchise_description, franchise_director) VALUES ('Marvel', 'heroes', 'simon');
INSERT INTO FRANCHISE (franchise_name, franchise_description, franchise_director) VALUES ('James Bond', 'man on a mission', 'ahmed');
INSERT INTO FRANCHISE (franchise_name, franchise_description, franchise_director) VALUES ('pirate of caribbean', 'pirates', 'sean');


INSERT INTO MOVIE (movie_title,movie_genre,movie_release_year, movie_director,movie_picture,movie_trailer,franchise_id) VALUES ('Ahmed hero of the jungle', 'action',2022,'simon','http://google/forrest','http://youtube.com/Ahmed/movies', 1);
INSERT INTO MOVIE (movie_title,movie_genre,movie_release_year,movie_director,movie_picture,movie_trailer,franchise_id) VALUES ('simon the pirate of the sea', 'adventure',2021,'ahmed','http://google/sea', 'http://youtube.com/simon/movies', 2);
INSERT INTO MOVIE (movie_title,movie_genre,movie_release_year,movie_director,movie_picture,movie_trailer,franchise_id) VALUES ('men in black', 'action',2020,'sean','http://google/suits', 'http://youtube.com/sean/movies', 3);

INSERT INTO CHARACTER (character_name,character_alias,character_gender,character_picture) VALUES ('ahmed', 'hulk', 'male','http://google/most-handsome');
INSERT INTO CHARACTER (character_name,character_alias,character_gender,character_picture) VALUES ('simon', 'james', 'male','http://google/famous-character');
INSERT INTO CHARACTER (character_name,character_alias,character_gender,character_picture) VALUES ('sean', 'traverymedia', 'male','http/google/famous-programmer');



INSERT INTO MOVIE_CHARACTER (movie_id, character_id) VALUES (1,1);
INSERT INTO MOVIE_CHARACTER (movie_id, character_id) VALUES (2,2);
INSERT INTO MOVIE_CHARACTER (movie_id, character_id) VALUES (3,3)